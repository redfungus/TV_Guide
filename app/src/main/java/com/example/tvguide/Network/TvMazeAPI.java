package com.example.tvguide.Network;

import com.example.tvguide.POJOs.scheduleitems.ScheduleItem;
import com.example.tvguide.POJOs.showitems.ShowItem;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;
import rx.Observable;

/**
 * Created by RF on 2/9/2016.
 */
public interface TvMazeAPI {

    @GET("/schedule")
    Call<ArrayList<ScheduleItem>> getTodaysSchedule();

    @GET("/schedule")
    Observable<ArrayList<ScheduleItem>> getTodaysScheduleRx();

    @GET("/search/shows")
    Call<ArrayList<ShowItem>> getSearchResult(@Query("q") String query);

    @GET("/search/shows")
    Observable<ArrayList<ShowItem>> getSearchResultRx(@Query("q") String query);

}
