package com.example.tvguide.Views;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.tvguide.Adapters.MainAdapter;
import com.example.tvguide.ComponentCache;
import com.example.tvguide.DaggerModules.MainPresenterComponent;
import com.example.tvguide.Interfaces.RecyclerViewOnItemClickListener;
import com.example.tvguide.MyApp;
import com.example.tvguide.POJOs.MainAdapterItem;
import com.example.tvguide.R;

import java.util.ArrayList;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;
import hugo.weaving.DebugLog;

public class MainActivity extends MainViewAbstract implements SwipeRefreshLayout.OnRefreshListener, RecyclerViewOnItemClickListener, SearchView.OnQueryTextListener, SearchView.OnCloseListener {

    private MainAdapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    @Bind(R.id.recycler_view_main) RecyclerView mRecyclerView;
    @Bind(R.id.searchView) SearchView mSearchView;
    @Bind(R.id.textView_title) TextView mTitleTextView;
    @Bind(R.id.swipe_container) SwipeRefreshLayout swipeRefreshLayout;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);

        //Find Views
        ButterKnife.bind(this);

        //Initialize
        swipeRefreshLayout.setOnRefreshListener(this);

        mRecyclerView.setHasFixedSize(true);
        mLayoutManager = new GridLayoutManager(this, 2, LinearLayoutManager.VERTICAL, false);
        mRecyclerView.setLayoutManager(mLayoutManager);

        mSearchView.setOnQueryTextListener(this);
        mSearchView.setOnCloseListener(this);

        mAdapter = new MainAdapter(presenter);
        mRecyclerView.setAdapter(mAdapter);

        presenter.bindView(this);
        presenter.init();
    }

    @Override
    public void onGetItemData(MainAdapterItem item) {
        Intent intent = new Intent(this, ShowActivity.class);
        intent.putExtra("id", item.getShow().getId());
        startActivity(intent);
    }

    @Override
    public void onGetDataList(ArrayList<MainAdapterItem> data) {
        mAdapter.setData(data);
    }

    @Override
    public void onLoadMoreData(MainAdapterItem item) {
        mAdapter.addItem(item);
    }

    @Override
    public void onSetTitle(String title) {
        mTitleTextView.setText(title);
    }

    @Override
    public void onRefresh() {
        presenter.refreshData();
    }

    @Override
    public void onStopRefresh() {
        swipeRefreshLayout.setRefreshing(false);
    }

    @Override
    public void onItemClick(int position) {

    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    @DebugLog
    @Override
    public boolean onQueryTextChange(String newQuery) {
        if(!newQuery.equals(""))
            presenter.searchQuery(newQuery);
        return true;
    }

    @Override
    public boolean onClose() {
        presenter.refreshData();
        return true;
    }
}
