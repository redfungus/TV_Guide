package com.example.tvguide.Views;

import android.os.Bundle;

import com.example.tvguide.ComponentCache;
import com.example.tvguide.ComponentManager;
import com.example.tvguide.DaggerModules.MainPresenterComponent;
import com.example.tvguide.POJOs.MainAdapterItem;
import com.example.tvguide.Presenters.MainPresenterAbstract;

import java.util.ArrayList;

/**
 * Created by RF on 2/1/2016.
 */
public abstract class MainViewAbstract extends ViewBaseActivity<MainPresenterAbstract>{

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        presenterKey = ComponentManager.MAIN_PRESENTER_ID;
        ((MainPresenterComponent) ComponentCache.getInstance().getComponent(presenterKey)).inject(this);
        //TODO can be modulized better to remove the need of explicitly naming component
    }

    public abstract void onGetDataList(ArrayList<MainAdapterItem> data);

    public abstract void onLoadMoreData(MainAdapterItem item);

    //    public void onItemSelected(int position);

    public abstract void onSetTitle(String title);

    public abstract void onStopRefresh();

    public abstract void onGetItemData(MainAdapterItem item);
}
