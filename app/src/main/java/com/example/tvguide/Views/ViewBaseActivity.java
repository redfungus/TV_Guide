package com.example.tvguide.Views;

import android.app.Activity;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

import com.example.tvguide.ComponentCache;
import com.example.tvguide.MyApp;
import com.example.tvguide.Presenters.Presenter;

import javax.inject.Inject;

import icepick.Icepick;

/**
 * Created by RF on 4/19/2016.
 */
public abstract class ViewBaseActivity<P extends Presenter> extends AppCompatActivity implements ViewBase{

    @Inject
    P presenter;
    //Activity Presenter
    //get Component from component cache and inject with it
    //and cast to proper component
    //e.g(ExtendedClassComponent) ComponentCache.getcomponent(presenterkey).inject(this)

    boolean isDestroyedBySystem = false;
    //Activity Presenter Key
    //assign value in your extended class
    String presenterKey;


    //TODO change hierarchy to be more simple

    @Override
    public void onCreate(Bundle savedInstanceState, PersistableBundle persistentState) {
        super.onCreate(savedInstanceState, persistentState);
        Icepick.restoreInstanceState(this, savedInstanceState);

    }

    @Override
    public Activity onGetActivity() {
        return this;
    }

    @Override
    public void toast(String msg) {
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        isDestroyedBySystem = true;
        Icepick.saveInstanceState(this, outState);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        presenter.unbindView();
        if(!isDestroyedBySystem){
            ComponentCache.getInstance().removeComponent(presenterKey);
        }
    }
}
