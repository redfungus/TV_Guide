package com.example.tvguide.Views;

import android.app.Activity;

import com.example.tvguide.Presenters.Presenter;

import javax.inject.Inject;

/**
 * Created by RF on 4/19/2016.
 */
public interface ViewBase {
    void toast(String msg);

    Activity onGetActivity();
}
