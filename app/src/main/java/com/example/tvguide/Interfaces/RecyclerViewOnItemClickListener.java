package com.example.tvguide.Interfaces;

/**
 * Created by RF on 2/25/2016.
 */
public interface RecyclerViewOnItemClickListener {
    public void onItemClick(int position);
}
