package com.example.tvguide;

import android.app.Application;

import com.example.tvguide.DaggerModules.AppModule;
import com.example.tvguide.DaggerModules.DaggerNetComponent;
import com.example.tvguide.DaggerModules.NetComponent;
import com.example.tvguide.DaggerModules.NetModule;

/**
 * Created by RF on 2/8/2016.
 */
public class MyApp extends Application{

    public static AppModule appModule;

    @Override
    public void onCreate() {
        super.onCreate();
//        LeakCanary.install(this);
    }

    public NetComponent getNetComponent() {
            return DaggerNetComponent.builder()
                    .appModule(new AppModule(this))
                    .netModule(new NetModule("http://api.tvmaze.com"))
                    .build();
    }
}
