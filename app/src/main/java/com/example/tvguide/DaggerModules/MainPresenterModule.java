package com.example.tvguide.DaggerModules;

import com.example.tvguide.Presenters.MainPresenterAbstract;
import com.example.tvguide.Presenters.MainPresenterImpl;
import com.example.tvguide.Views.MainViewAbstract;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by RF on 2/5/2016.
 */

@Module
public class MainPresenterModule {

    @Provides
    @Singleton
    MainPresenterAbstract provideMainPresenter(){
        return new MainPresenterImpl();
    }

}
