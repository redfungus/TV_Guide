package com.example.tvguide.DaggerModules;

import android.app.Application;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by RF on 2/9/2016.
 */

@Module
public class AppModule {

    Application mApplication;

    public AppModule(Application application){
        this.mApplication = application;
    }

    @Provides
    @Singleton
    Application provideApplication(){
        return this.mApplication;
    }

}


