package com.example.tvguide.DaggerModules;

import com.example.tvguide.Views.MainViewAbstract;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Created by RF on 2/8/2016.
 */

@Singleton
@Component(modules = {MainPresenterModule.class})
public interface MainPresenterComponent {
    void inject(MainViewAbstract mainView);
}
