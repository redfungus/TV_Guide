package com.example.tvguide.DaggerModules;

import com.example.tvguide.Presenters.MainPresenterImpl;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Created by RF on 2/9/2016.
 */
@Singleton
@Component(modules={AppModule.class, NetModule.class})
public interface NetComponent {
    void inject(MainPresenterImpl mainPresenter);
}
