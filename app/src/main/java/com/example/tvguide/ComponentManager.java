package com.example.tvguide;

import com.example.tvguide.DaggerModules.DaggerMainPresenterComponent;
import com.example.tvguide.DaggerModules.MainPresenterComponent;
import com.example.tvguide.DaggerModules.MainPresenterModule;

/**
 * Created by RF on 4/22/2016.
 */
public class ComponentManager {

    //define your presenter component keys here

    public final static String MAIN_PRESENTER_ID = "mainpresentercomponent";

    //TODO make generic component class to remove concern of defining keys explicitly

    public static Object build(String key){

        switch (key){
            //add component building procedure here
            case MAIN_PRESENTER_ID:
                return DaggerMainPresenterComponent.builder()
                        .mainPresenterModule(new MainPresenterModule())
                        .build();
        }
        return null;
    }


}
