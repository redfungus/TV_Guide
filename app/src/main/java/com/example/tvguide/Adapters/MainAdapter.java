package com.example.tvguide.Adapters;

import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.tvguide.Interfaces.RecyclerViewOnItemClickListener;
import com.example.tvguide.POJOs.MainAdapterItem;
import com.example.tvguide.Presenters.MainPresenterAbstract;
import com.example.tvguide.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by RF on 2/3/2016.
 */
public class MainAdapter extends RecyclerView.Adapter<MainAdapter.ViewHolder> {
    ArrayList<MainAdapterItem> data;
    @Inject
    MainPresenterAbstract mainPresenter;
    RecyclerViewOnItemClickListener onItemClickListener;

    public MainAdapter(MainPresenterAbstract mainPresenter) {
        this.data = new ArrayList<>();
        this.mainPresenter = mainPresenter;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public MainAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                     int viewType) {
        // create a new view
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_main_grid, parent, false);
        // set the view's size, margins, paddings and layout parameters
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element

        try {
            holder.mTextView_showName.setText(data.get(position).getShow().getName());
            Log.d("Show name", data.get(position).getShow().getName());
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            Picasso.with(mainPresenter.getActivity().getApplicationContext()).load(data.get(position).getShow().getImage().getOriginal()).resize(150, 150).into(holder.mImageView_showImage);
        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return data.size();
    }

    public ArrayList<MainAdapterItem> getData() {
        return data;
    }

    public void setData(ArrayList<MainAdapterItem> data) {
        if (data != null && data.size() > 0) {
            this.data.clear();
            this.data.addAll(data);
            notifyDataSetChanged();
        }
    }

    public void addItem(MainAdapterItem item) {
        data.add(item);
        notifyDataSetChanged();
    }

    public void removeItem(int position) {
        if (position >= 0 && position < data.size()) {
            data.remove(position);
            notifyDataSetChanged();
        }
    }

    public void setOnItemClickListener(RecyclerViewOnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        @Bind(R.id.textView_series_name)
        TextView mTextView_showName;
        @Bind(R.id.imageView_series_image)
        ImageView mImageView_showImage;

        public ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
            view.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            mainPresenter.loadItemData(this.getAdapterPosition());
        }
    }
}