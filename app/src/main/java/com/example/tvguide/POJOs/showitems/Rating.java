
package com.example.tvguide.POJOs.showitems;

import javax.annotation.Generated;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

@Generated("org.jsonschema2pojo")
public class Rating {

    @SerializedName("average")
    @Expose
    private Object average;

    /**
     * 
     * @return
     *     The average
     */
    public Object getAverage() {
        return average;
    }

    /**
     * 
     * @param average
     *     The average
     */
    public void setAverage(Object average) {
        this.average = average;
    }

}
