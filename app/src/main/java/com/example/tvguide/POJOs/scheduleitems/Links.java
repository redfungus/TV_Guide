
package com.example.tvguide.POJOs.scheduleitems;

import javax.annotation.Generated;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

@Generated("org.jsonschema2pojo")
public class Links {

    @SerializedName("self")
    @Expose
    private Self self;
    @SerializedName("previousepisode")
    @Expose
    private Previousepisode previousepisode;
    @SerializedName("nextepisode")
    @Expose
    private Nextepisode nextepisode;

    /**
     * 
     * @return
     *     The self
     */
    public Self getSelf() {
        return self;
    }

    /**
     * 
     * @param self
     *     The self
     */
    public void setSelf(Self self) {
        this.self = self;
    }

    /**
     * 
     * @return
     *     The previousepisode
     */
    public Previousepisode getPreviousepisode() {
        return previousepisode;
    }

    /**
     * 
     * @param previousepisode
     *     The previousepisode
     */
    public void setPreviousepisode(Previousepisode previousepisode) {
        this.previousepisode = previousepisode;
    }

    /**
     * 
     * @return
     *     The nextepisode
     */
    public Nextepisode getNextepisode() {
        return nextepisode;
    }

    /**
     * 
     * @param nextepisode
     *     The nextepisode
     */
    public void setNextepisode(Nextepisode nextepisode) {
        this.nextepisode = nextepisode;
    }

}
