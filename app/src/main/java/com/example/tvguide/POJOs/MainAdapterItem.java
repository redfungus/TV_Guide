package com.example.tvguide.POJOs;

import com.example.tvguide.POJOs.showitems.Show;

/**
 * Created by RF on 4/16/2016.
 */
public interface MainAdapterItem {

    Show getShow();

}
