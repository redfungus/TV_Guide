package com.example.tvguide;

import com.example.tvguide.DaggerModules.DaggerMainPresenterComponent;
import com.example.tvguide.DaggerModules.MainPresenterModule;
import com.example.tvguide.DaggerModules.MainPresenterComponent;

import java.util.HashMap;

/**
 * Created by RF on 2/25/2016.
 */

public class ComponentCache {

    HashMap<String, Object> components = new HashMap<>();

    private static ComponentCache instance = null;

    public static ComponentCache getInstance() {
        if(instance == null){
            instance = new ComponentCache();

        }
        return instance;
    }

    public Object getComponent(String key){
        if(components.get(key) != null){
            return components.get(key);
        }
        else {
            Object component = ComponentManager.build(key);
            components.put(key, component);
            return component;
        }
    }

    public void removeComponent(String key){
        components.remove(key);
    }

}
