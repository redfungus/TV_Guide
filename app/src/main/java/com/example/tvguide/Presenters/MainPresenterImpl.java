package com.example.tvguide.Presenters;

import android.util.Log;

import com.example.tvguide.ComponentCache;
import com.example.tvguide.MyApp;
import com.example.tvguide.Network.TvMazeAPI;
import com.example.tvguide.POJOs.MainAdapterItem;
import com.example.tvguide.POJOs.scheduleitems.ScheduleItem;
import com.example.tvguide.POJOs.showitems.ShowItem;
import com.example.tvguide.Views.MainViewAbstract;

import java.util.ArrayList;

import javax.inject.Inject;

import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by RF on 2/1/2016.
 */
public class MainPresenterImpl extends MainPresenterAbstract {

    @Inject
    TvMazeAPI mTvMazeAPI;
    ArrayList<MainAdapterItem> data;
    private volatile boolean isLoading = false;

    public MainPresenterImpl() {
    }

    @Override
    public void init() {
        mainView.get().onSetTitle("Television Guide");
        ((MyApp)mainView.get().getApplication()).getNetComponent().inject(this);
        this.loadDataToView();
    }

    @Override
    public void loadDataToView() {
        if (data != null)
            mainView.get().onGetDataList(data);
        else
            refreshData();
    }

    @Override
    public void refreshData() {
        if (!isLoading) {
//            isLoading = true;
//            Call<ArrayList<ScheduleItem>> loadDataCall= mTvMazeAPI.getTodaysSchedule();
//            loadDataCall.enqueue(new Callback<ArrayList<ScheduleItem>>() {
//                @Override
//                public void onResponse(Call<ArrayList<ScheduleItem>> call, Response<ArrayList<ScheduleItem>> response) {
//                    data = new ArrayList<MainAdapterItem>(response.body());
//                    if (mainView != null) {
//                        mainView.get().onGetDataList(data);
//                        mainView.get().onStopRefresh();
//                        isLoading = false;
//                    }
//                }
//                @Override
//                public void onFailure(Call<ArrayList<ScheduleItem>> call, Throwable t) {
//                    if (mainView != null) {
//                        mainView.get().toast("Error on loading data!");
//                        mainView.get().onStopRefresh();
//                        t.printStackTrace();
//                        isLoading = false;
//                    }
//                }
//            });



            //Implementation using Rxjava in Retrofit
            isLoading = true;
            Observable<ArrayList<ScheduleItem>> requestObservable = mTvMazeAPI.getTodaysScheduleRx();

            requestObservable.subscribeOn(Schedulers.newThread())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Subscriber<ArrayList<ScheduleItem>>() {
                        @Override
                        public void onCompleted() {
                            Log.d("Main Data", "Retrieved ");
                        }

                        @Override
                        public void onError(Throwable e) {
                            if (mainView != null) {
                                mainView.get().toast("Error on loading data!");
                                mainView.get().onStopRefresh();
                                e.printStackTrace();
                                isLoading = false;
                            }
                        }

                        @Override
                        public void onNext(ArrayList<ScheduleItem> scheduleItems) {
                            data = new ArrayList<MainAdapterItem>(scheduleItems);
                            if (mainView != null) {
                                mainView.get().onGetDataList(data);
                                mainView.get().onStopRefresh();
                                isLoading = false;
                            }
                        }
                    });
        }
    }

    @Override
    public void searchQuery(String query) {

        if(!isLoading) {


            //Implementation using Rxjava in Retrofit
            isLoading = true;
            Observable<ArrayList<ShowItem>> requestObservable = mTvMazeAPI.getSearchResultRx(query);

            requestObservable.subscribeOn(Schedulers.newThread())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Subscriber<ArrayList<ShowItem>>() {
                        @Override
                        public void onCompleted() {
                            Log.d("Main Data", "Retrieved ");
                        }

                        @Override
                        public void onError(Throwable e) {
                            if (mainView != null) {
                                mainView.get().toast("Error on loading data!");
                                mainView.get().onStopRefresh();
                                e.printStackTrace();
                                isLoading = false;
                            }
                        }

                        @Override
                        public void onNext(ArrayList<ShowItem> showItems) {
                            data = new ArrayList<MainAdapterItem>(showItems);
                            if (mainView != null) {
                                mainView.get().onGetDataList(data);
                                mainView.get().onStopRefresh();
                                isLoading = false;
                            }
                        }
                    });



//            isLoading = true;
//            Call<ArrayList<ShowItem>> loadDataCall= mTvMazeAPI.getSearchResult(query);
//            loadDataCall.enqueue(new Callback<ArrayList<ShowItem>>() {
//                @Override
//                public void onResponse(Call<ArrayList<ShowItem>> call, Response<ArrayList<ShowItem>> response) {
//                    Log.d("Search Request", "onResponse + " + response.message() + response.code());
//                    data = new ArrayList<MainAdapterItem>(response.body());
//                    Log.d("Search Request", "Data Size: " + data.size() );
//                    if (mainView != null) {
//                        mainView.get().onGetDataList(data);
//                        isLoading = false;
//                    }
//                }
//
//                @Override
//                public void onFailure(Call<ArrayList<ShowItem>> call, Throwable t) {
//                    if (mainView != null) {
//                        mainView.get().toast("Error on loading data!");
//                        t.printStackTrace();
//                        isLoading = false;
//                    }
//                }
//            });
        }
    }

    @Override
    public void loadItemData(int position) {
        mainView.get().toast("Item Clicked");
        mainView.get().onGetItemData(data.get(position));
    }

}
