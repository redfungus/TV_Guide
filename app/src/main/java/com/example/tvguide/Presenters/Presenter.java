package com.example.tvguide.Presenters;

import android.app.Activity;

import com.example.tvguide.Views.ViewBase;

import java.lang.ref.WeakReference;

/**
 * Created by RF on 4/19/2016.
 */
public abstract class Presenter<V extends ViewBase> {

    WeakReference<V> mainView;

    public abstract void init();

    public void bindView(V view){
        this.mainView = new WeakReference<>(view);
    }

    public void unbindView(){
        if (mainView != null)
            mainView.clear();
        mainView = null;
    }

    public Activity getActivity(){
        return mainView.get().onGetActivity();
    }

}
