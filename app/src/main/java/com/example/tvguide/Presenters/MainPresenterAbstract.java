package com.example.tvguide.Presenters;

import com.example.tvguide.Views.MainViewAbstract;
import com.example.tvguide.Views.ViewBase;

/**
 * Created by RF on 2/1/2016.
 */
public abstract class MainPresenterAbstract extends Presenter<MainViewAbstract> {

    public abstract void loadItemData(int position);

    public abstract void loadDataToView();

    public abstract void refreshData();

    public abstract void searchQuery(String query);
}
