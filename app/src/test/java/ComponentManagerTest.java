import android.support.test.runner.AndroidJUnit4;
import android.test.suitebuilder.annotation.SmallTest;

import com.example.tvguide.Presenters.MainPresenterAbstract;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import static org.hamcrest.Matchers.instanceOf;
import static org.junit.Assert.assertThat;

/**
 * Created by RF on 4/22/2016.
 */

//
@RunWith(AndroidJUnit4.class)
@SmallTest
public class ComponentManagerTest {

    ComponentManager componentManager;

    @Before
    public void SetUp() {
        componentManager = new ComponentManager();
    }

    @Test
    public void getComponentTest() {
        Object component = ComponentManager.build(ComponentManager.MAIN_PRESENTER_ID);
        assertThat(component, instanceOf(MainPresenterAbstract.class));
    }


}
